import tkinter as tk
from tkinter import ttk
from utility_classes import *


class LabeledEntry(tk.Frame):
	"""
	A Labeled Entry that has a configurable label position
	"""

	def __init__(self, master=None, label_pos="top", cnf={}, **kw):
		text = kw.pop("text")
		super().__init__(master, cnf, **kw)
		self.label = tk.Label(self, text=text)
		self.label.pack(side=label_pos)
		self.entry = tk.Entry(self)
		self.entry.pack(side="top")

	def get(self) -> str:
		"""
		Returns value of entry
		:return: Value of entry
		"""
		return self.entry.get()

	def set_label(self, text: str) -> None:
		"""
		Set the value of label
		:param text: The label text
		:return: None
		"""
		self.label["text"] = text

	def set_text(self, text: str, should_lock=False) -> None:
		"""
		Set text of the Entry
		:param should_lock:
		:param text: The text to set the entry for
		:return: None
		"""
		self.entry.config(state="normal")
		self.entry.delete(0, len(self.get()))
		self.entry.insert(0, text)
		if should_lock:
			self.entry.config(state="readonly")

	def get_label(self):
		return self.label["text"]


class ListView(ttk.Treeview):

	def __init__(self, callback=None, master=None, **kw):
		super().__init__(master, **kw)
		self["show"] = "headings"
		self.bind("<<TreeviewSelect>>", self.handle_click)
		self.callback = callback

	def handle_click(self, event):
		self.selection_clear()
		print(event)
		if self.callback:
			self.callback(event)

	def get(self):
		return self.item(self.selection(), "values")

	def add_all(self, iterable: iter):
		"""
		Add all values from a iterable
		:param iterable: The iterable to add the values from
		:return: None
		"""
		values = iter(iterable)
		for x in values:
			self.insert("", "end", values=x)


categories = {"Name": 1, "Platform": 2, "Year of release": 3, "Genre": 4, "Publisher": 5, "Rating": 16}
# Relates pretty names to database names
review_fields = {"Name": "name", "Developer": "developer", "Critic score": "critic_score", "User score": "user_score",
                 "Publisher": "publisher", "Year of Release": "year_of_release", "Critic count": "critic_count",
                 "User count": "user_count", "NA Sales": "na_sales", "EU Sales": "eu_sales", "JP Sales": "jp_sales"}


class UserSearchPage(tk.Frame):
	def __init__(self, master=None, cnf={}, **kw):
		super().__init__(master, cnf, **kw)
		self.all_articles = self.master.database.get_articles()
		self.search_bar = tk.Frame(self)
		self.search_entry_string = tk.StringVar()
		self.search_entry = LabeledEntry(master=self.search_bar, label_pos="left", text="Search: ")
		self.search_entry.entry["textvariable"] = self.search_entry_string
		self.search_entry.pack(side="left", pady="4px", padx="4px")
		self.search_bar.pack(side="top", fill="x")
		self.search_cat = ttk.Combobox(self.search_bar, values=list(categories.keys()))
		self.search_cat.pack(side="left")
		self.search_entry.entry.bind("<Key>", lambda event: self.show_articles(self.search_cat.get(),
		                                                                       self.search_entry_string.get()))
		self.ListView = ListView(master=self, columns=list(categories.keys()), selectmode="browse")
		for column in self.ListView["columns"]:
			self.ListView.heading(column, text=column)
		self.ListView.pack(side="top", fill="x")
		self.show_articles()
		self.select_button = tk.Button(self, text="Select", command=self.listview_callback)
		self.select_button.pack()

	def listview_callback(self):
		self.master.get_page("user_review_page").set_game(self.ListView.get()[-1])
		self.master.switch_page("user_review_page")

	def show_articles(self, category="", search=""):
		self.ListView.delete(*self.ListView.get_children())
		if (not category) or (not search):
			self.ListView.add_all(self.master.database.execute_query(f"""
						SELECT
							name, platform, year_of_release, genre, publisher, rating, game_id
						FROM
							game_data
						""")[1:])
			return

		self.ListView.add_all(self.master.database.execute_query(f"""
			SELECT
				name, platform, year_of_release, genre, publisher, rating, game_id
			FROM
				game_data
			WHERE
				`{category}` LIKE '%{search}%'
			""")[1:])


class UserReviewPage(tk.Frame):
	def __init__(self, master=None, cnf={}, **kw):
		super().__init__(master, cnf, **kw)
		self.game_data = {}
		self.game_id = -1
		self.rows = []
		self.entries = {}
		count = 0
		for rows in range(len(review_fields) // 4 + 1):
			frame = tk.Frame(self)
			self.rows.append(frame)
			frame.pack(side="top", fill="x")

		for x in review_fields:
			self.entries[x] = LabeledEntry(self.rows[(count // 4)], label_pos="left", text=x)
			self.entries[x].pack(side="left", padx="15px")
			count += 1

		self.cancel_button = tk.Button(self, text="Cancel", command=lambda: self.master.switch_page("oof"))
		self.cancel_button.pack(side="bottom", pady="2px")
		self.submit_button = tk.Button(self, text="Submit", command=self.submit)
		self.submit_button.pack(side="bottom", pady="2px")
		self.article_review = tk.Text(self)
		self.article_review.pack(pady="15px", padx="15px", fill="both")

	def set_game(self, game_id):
		self.game_id = game_id
		self.game_data = \
			self.master.database.execute_query("SELECT * FROM game_data WHERE game_id = ?", (game_id,), use_dict=True)[
				0]
		for key, value in self.entries.items():
			value.set_text(self.game_data[review_fields[key]], should_lock=self.master.user.permission_level < 2)

	def submit(self):
		if self.master.user.permission_level >= 2:
			for x in self.entries.values():
				if x.get():
					# print(f"UPDATE game_data SET {review_fields[x.get_label()]} = '{x.get()}' WHERE game_id = {self.game_id}")
					self.master.database.execute_query(
						f"UPDATE game_data SET {review_fields[x.get_label()]} = '{x.get()}' WHERE game_id = {self.game_id}")

		self.master.database.create_article(self.master.user.id, self.game_id, self.article_review.get("1.0"))


class SupervisorPage(ttk.Notebook):

	def __init__(self, master=None, **kw):
		super().__init__(master, **kw)
		# >>--------------------<< Sign Up Page >>--------------------<< #
		self.signup_page = tk.Frame(self)
		self.signup_page.name_input = LabeledEntry(self.signup_page, text="First Name")
		self.signup_page.name_input.pack(pady="10px")
		self.signup_page.surname_input = LabeledEntry(self.signup_page, text="Surname Name")
		self.signup_page.surname_input.pack(pady="10px")
		self.signup_page.email_input = LabeledEntry(self.signup_page, text="Email")
		self.signup_page.email_input.pack(pady="10px")
		self.signup_page.password_input = LabeledEntry(self.signup_page, text="Password")
		self.signup_page.password_input.pack(pady="10px")
		self.signup_page.power_level_label = tk.Label(self.signup_page, text="Permission Level")
		self.signup_page.power_level_input = tk.Scale(self.signup_page, from_=1, to=3, orient="horizontal")
		self.signup_page.power_level_input.pack()
		self.signup_page.submit_button = tk.Button(self.signup_page, text="Create", command=self.attempt_signup)
		self.signup_page.submit_button.pack(pady="5px")
		self.add(self.signup_page, text="User Creation")

		# >>--------------------<< Article publishing >>--------------------<< #

	def attempt_signup(self):
		# Handle if wasn't made. Likely due to matching email
		if not self.master.database.add_user(
				self.signup_page.name_input.get(), self.signup_page.surname_input.get(),
				self.signup_page.email_input.get(),
				self.signup_page.password_input.get(), self.signup_page.power_level_input.get()):

			self.master.do_error("Email already exists!")

		else:
			self.master.do_error("User Created!")


class LoginPage(tk.Frame):
	def __init__(self, callback=None, master=None, cnf={}, **kw):
		super().__init__(master, cnf, **kw)
		self.username_input = LabeledEntry(self, text="Username:")
		self.username_input.pack(side="top")
		self.password_input = LabeledEntry(self, text="Password:")
		self.password_input.pack(side="top", pady="5px")
		self.login_button = tk.Button(self, command=self.login_attempt, text="Login")
		self.login_button.pack(side="top")
		self.error_label = tk.Label(self, fg="red")
		self.error_label.pack(pady="20px")
		self.callback = callback

	def set_callback(self, callback: Union[staticmethod, classmethod]) -> None:
		"""
		Sets the call back function
		:param callback: function to call, returns either False or User object
		:return:
		"""
		self.callback = callback

	def login_attempt(self) -> None:
		"""
		Attempt login
		:return: None
		"""
		if (not self.username_input.get()) or (not self.password_input.get()):
			self.raise_error("Put in a user name and password.")
			return

		attempt = self.master.database.user_login(self.username_input.get(), self.password_input.get())
		if not attempt:
			self.raise_error("Incorrect username or password!")
		else:
			self.reset_error()
			self.callback(attempt)

	# try:
	# 	self.callback(attempt)
	# except TypeError:
	# 	print("Someone hasn't added a callback!")

	def raise_error(self, text) -> None:
		"""
		Sets red text as to show error
		:param text: Text Error
		:return:
		"""
		self.error_label["text"] = text

	def reset_error(self) -> None:
		"""
		Resets the error text
		:return:
		"""
		self.error_label["text"] = ""


class Window(tk.Tk):
	def __init__(self, database: sqlite3.Connection, screenName=None, baseName=None, className="Club Thing", useTk=True,
	             sync=False, use=None):
		super().__init__(screenName, baseName, className, useTk, sync, use)
		# Dict to store all the "pages"
		self.currentPage = None
		self.pages = {}

		self.database = database
		self.geometry("500x500")
		self.add_page("login_page", LoginPage(self))

		self.get_page("login_page").set_callback(self._on_login)
		self.add_page("oof", UserSearchPage(self))
		self.switch_page("login_page")
		self.add_page("user_review_page", UserReviewPage(self))
		self.add_page("admin_page", SupervisorPage(self))

	def add_page(self, key: str, page: tk.Widget) -> tk.Widget:
		"""
		Adds a page so it can be switched to
		:param key: string to index page with
		:param page: The page to add, a widget
		:return: page
		"""
		self.pages[key] = page
		return page

	def get_page(self, key) -> tk.Widget:
		"""
		Returns page with given key
		:param key: index to retrieve page with
		:return:
		"""
		return self.pages[key]

	def switch_page(self, key) -> None:
		"""
		Display the page with given key
		:param key: index to switch page with
		:return: None
		"""
		if self.currentPage:
			self.currentPage.pack_forget()
		page = self.get_page(key)
		page.pack(fill="both")
		self.currentPage = page

	def do_error(self, message: str):
		window = tk.Toplevel(self)
		window.title("uh oh!")
		window.geometry("300x200")
		error_label = tk.Label(window, fg="red", text=message)
		error_label.pack()
		close = tk.Button(window, text="OK", command=lambda: window.destroy())
		close.pack()

	def _on_login(self, user: User):
		"""
		INTERNAL: attempt login user
		:param user:
		:return:
		"""
		self.user = user
		self.switch_page("oof")
		self.get_page("oof").show_articles()
		if user.permission_level >= 2:
			self.get_page("oof").supervisor_button = tk.Button(self.get_page("oof"), text="Admin",
			                                                   command=lambda: self.switch_page("admin_page"))
			self.get_page("oof").supervisor_button.pack()
