import csv
import sqlite3
from typing import Union


# Hack to prevent errors
class User:
	pass


def dict_factory(cursor, row):
	d = {}
	for idx, col in enumerate(cursor.description):
		d[col[0]] = row[idx]
	return d


class Database:
	def __init__(self, path: str) -> None:
		self.path = path
		self.__create_connection(path)

	def get_articles(self) -> list:
		"""
		Returns all tables in database
		:return:
		"""
		return self.execute_query("SELECT * FROM game_data")

	def __create_connection(self, path: str) -> sqlite3.Connection:
		"""
		INTERNAL: Creates a connection object for a sqlite database at the given path
		:param path: A path leading to the sqlite database
		:return: A connection object for the sqlite database
		"""
		connection = None
		try:
			connection = sqlite3.connect(path)
		# print("Connection to SQLite DB successful")
		except sqlite3.Error as e:
			print(f"The error '{e}' occurred")

		self.connection = connection
		return connection

	def execute_query(self, query: str, params=(), use_dict=False) -> Union[list, dict, sqlite3.Error]:
		"""
		A helper function that executes SQL queries for the sqlite connection passed.
		:param query: sqlite query
		:param params: Parameters to execute query with
		:param use_dict: True to return the query in a dict object
		:return: List containing retrieved data.
		"""
		cursor = self.connection.cursor()
		if use_dict:
			cursor.row_factory = dict_factory
		try:
			cursor.execute(query, params)
			self.connection.commit()
			# print("Query executed successfully")
			return cursor.fetchall() or []
		except sqlite3.Error as e:
			print(f"The error '{e}' occurred")
			return e

	def add_user(self, first_name: str, last_name: str, email:str, password: str, power_level: int) -> bool:
		"""
		Adds user to the user table
		:param username: The user's username
		:param password: The user's password
		:param power_level: The user's power level
		:return: true if successful
		"""
		if self.execute_query(f"SELECT * FROM users WHERE email = '{email}' LIMIT 1"):
			print("User with email already exists!")
			return False

		return (type(
			self.execute_query("INSERT INTO users(first_name, last_name, email, password, permission_level) VALUES (?, ?, ?, ?, ?)",
			                   (first_name, last_name, email, password, power_level))) != sqlite3.Error)

	def remove_user(self, user_id: int):
		"""
		Removes user from database
		:param user_id: user id to remove
		:return: None
		"""
		self.execute_query("DELETE FROM users WHERE user_id = ?", (user_id,))

	def get_user(self, user_id=None, username=None):
		user_data = None
		if user_id:
			query_data = self.execute_query("SELECT * FROM users WHERE user_id = ?", params=(user_id,), use_dict=True)
			if len(query_data) > 0:
				user_data = query_data[0]
		elif username:
			query_data = self.execute_query("SELECT * FROM users WHERE email = ?", params=(username,), use_dict=True)
			if len(query_data) > 0:
				user_data = query_data[0]
		else:
			raise Exception("You must use either user_id or username!")
		if not user_data:
			return None
		return user_data["password"], User(user_data["user_id"], user_data["first_name"], user_data["last_name"], user_data["email"], user_data["permission_level"],
		                                   self)

	def update_user(self, user_id, username=None, password=None, permission_level=None):
		if username:
			self.execute_query("UPDATE users SET username = ? WHERE user_id = ?", (username, user_id))
		if password:
			self.execute_query("UPDATE users SET password = ? WHERE user_id = ?", (password, user_id))
		if permission_level:
			self.execute_query("UPDATE users SET permission_level = ? WHERE user_id = ?", (permission_level, user_id))

	def user_login(self, username, password) -> Union[bool, User]:
		user = self.get_user(username=username)
		if user is None or password != user[0]:
			return False
		else:
			return self.get_user(username=username)[1]

	def create_article(self, user, game, text):
		# Check if there is already a game here. If there is update, otherwise create it.
		if self.execute_query("SELECT * FROM user_articles WHERE user_id = ? AND game_id = ?", (user, game)):
			self.execute_query("UPDATE user_articles SET article = ? WHERE user_id = ? AND game_id = ?", (text, user, game))
		else:
			self.execute_query("INSERT INTO user_articles (user_id, game_id, article) VALUES (?, ?, ?)",
			                   (user, game, text))

	def toggle_publish(self, user, game) -> bool:
		self.execute_query("UPDATE user_articles SET published = 1 -(SELECT published from user_articles"
		                   " WHERE user_id = ? AND game_id = ?) WHERE user_id = ? AND game_id = ?",
		                   (user, game, user, game))
		return \
		self.execute_query("SELECT published FROM user_articles WHERE user_id = ? AND game_id = ?", (user, game))[0][
			0] == 1

	def import_csv(self, path):
		values_to_insert = []
		with open(path, newline='') as csvfile:
			reader = csv.reader(csvfile, quotechar="\"")

			for row in reader:
				values_to_insert.append(tuple(row))
			self.connection.executemany(
				"INSERT OR IGNORE INTO game_data(name, platform, year_of_release, genre, publisher, na_sales, eu_sales,"
				"jp_sales, other_sales, global_sales, critic_score, critic_count, user_score, user_count, "
				"developer, rating) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
				values_to_insert[1:])



def setup_database(database: Database, csvpath: str) -> None:
	"""
	Sets up the needed tables for the database to  be used in the program and imports the csv file.
	:param database: The database objective to setup
	:param csvpath: The path to the csv file to import
	:return: None
	"""

	# It's fine to overwrite table?
	database.connection.execute("DROP TABLE IF EXISTS game_data")
	database.connection.executescript("""
		CREATE TABLE game_data(
			game_id             INTEGER PRIMARY KEY,
			name                TEXT NOT NULL,
			platform            TEXT NOT NULL,
			year_of_release     INTEGER NOT NULL,
			genre               TEXT NOT NULL,
			publisher           TEXT NOT NULL,
			na_sales            REAL NOT NULL,
			eu_sales            REAL NOT NULL,
			jp_sales            REAL NOT NULL,
			other_sales         REAL NOT NULL,
			global_sales        REAL NOT NULL,
			critic_score        INTEGER DEFAULT NULL,
			critic_count        INTEGER DEFAULT NULL,
			user_score          REAL DEFAULT NULL,
			user_count          INTEGER DEFAULT NULL,
			developer           TEXT DEFAULT NULL,
			rating              TEXT NOT NULL
		); CREATE TABLE IF NOT EXISTS users(
			user_id             INTEGER PRIMARY KEY,
			first_name          TEXT NOT NULL,
			last_name           TEXT NOT NULL,
			email               TEXT NOT NULL UNIQUE,
			password            TEXT NOT NULL,
			permission_level    INTEGER DEFAULT 1,
			last_update         TIMESTAMP DEFAULT (datetime('now', 'localtime'))
		); CREATE TABLE IF NOT EXISTS user_articles(
			user_id             INTEGER,
			game_id             INTEGER,
			creation_date       TIMESTAMP default (datetime('now', 'localtime')),
			article             TEXT NOT NULL,
			published           INTEGER DEFAULT 0,
			PRIMARY KEY (user_id, game_id),
			FOREIGN KEY (user_id)
			references users(user_id),
			FOREIGN KEY (game_id) references game_data(game_id));
	""")

	database.import_csv("game.csv")

	# Test if there are any logins in the database
	if not database.execute_query("SELECT * FROM users LIMIT 1"):
		print("No users detected, adding default root user")
		database.add_user("root", "root", "root", "root", 9001)
	database.connection.commit()


class User:
	def __init__(self, user_id: int, first_name: str, last_name: str, email: str, permission_level: int, database: Database):
		self.id = user_id
		self.first_name = first_name
		self.last_name = last_name
		self.email = email
		self.permission_level = permission_level
		self.database = database
		self.articles = self.get_articles()
		self.writen_game_ids = self.get_writen_game_ids()

	def save(self):
		self.database.update_user(self.id, username=self.username, permission_level=self.permission_level)

	def get_articles(self):
		return self.database.execute_query("SELECT * FROM user_articles WHERE user_id = ?", (self.id,))

	def get_writen_game_ids(self):
		return self.database.execute_query("SELECT game_id FROM user_articles WHERE user_id = ?", (self.id,))

	def is_supervisor(self):
		return self.permission_level >= 3

	def is_leader(self):
		return self.permission_level >= 2


class Article:
	def __init__(self, game_id: int, text: str, author: User):
		self.game_id = game_id
		self.text = text
		self.author = author


if __name__ == "__main__":
	setup_database(Database("../csvToSQLite/games.sqlite"), "game.csv")
